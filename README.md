# WikiToLearn Shibboleth Discovery Service

## Synopsis
JavaEE service that updates periodically Keycloak IdPs from a WAYF file.
It is originally forked from [Ortolang/discovery](https://github.com/Ortolang/discovery).

## How to run
### Docker setup
A `docker-compose.yml` was provided to run Keycloak and the WikiToLearn Shibboleth Discovery
Service.

It is strongly recommended to run Keycloak before the WikiToLearn Shibboleth Discovery Service
even if `depends_on` was declared.

In order to provide application properties follows the following steps.

- Copy the `application.yml.example` file within the proper config directory with this command:
```
cp src/main/resources/application.yml.example config/shibboleth-discovery/application.yml
```
- Change the file accordingly to your needs.

### Manual setup
Create the `.jar` file running `mvn spring-boot:repackage -DskipTests` or the Maven command which fit
your needs.

Then run the application with the following command replacing `{version}` with the current version:

```
java -jar target/shibboleth-discovery-{version}.jar --spring.config.location=file:./config/shibboleth-discovery/
```

NB: During the application startup the first synchronization process is run.
Consiquently, you should provide valid Keycloak properties if you want this process
succeeded.

## License
This project is licensed under the LGPLv3. See the [LICENSE.md](LICENSE.md) file for details.
