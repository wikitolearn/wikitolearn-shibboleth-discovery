package org.wikitolearn.shibbolethdiscovery.config;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Properties specific to Shibboleth Discovery.
 * Properties are configured in the application.yml file.
 *
 * @author Alessandro Tundo
 */
@Configuration
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {

  private final Keycloak keycloak = new Keycloak();

  private final Wayf wayf = new Wayf();

  private final Idp idp = new Idp();

  private final Sp sp = new Sp();

  private String syncRate = "3600000";

  private String preferredLanguage = "en";

  public Keycloak getKeycloak() {
    return keycloak;
  }

  public Wayf getWayf() {
    return wayf;
  }

  public Idp getIdp() {
    return idp;
  }

  public Sp getSp() {
    return sp;
  }

  public static class Keycloak {
    private String username = "username";

    private String password = "password";

    private String realm = "realm";

    private String masterRealm = "master";

    private String client = "client";

    private String url = "url";

    private String publicUrl = "publicUrl";

    /**
     * @return the username
     */
    public String getUsername() {
      return username;
    }

    /**
     * @param username
     *            the username to set
     */
    public void setUsername(String username) {
      this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
      return password;
    }

    /**
     * @param passowrd
     *            the password to set
     */
    public void setPassword(String password) {
      this.password = password;
    }

    /**
     * @return the realm
     */
    public String getRealm() {
      return realm;
    }

    /**
     * @param realm
     *            the realm to set
     */
    public void setRealm(String realm) {
      this.realm = realm;
    }

    /**
     * @return the masterRealm
     */
    public String getMasterRealm() {
      return masterRealm;
    }

    /**
     * @param masterRealm
     *            the masterRealm to set
     */
    public void setMasterRealm(String masterRealm) {
      this.masterRealm = masterRealm;
    }

    /**
     * @return the client
     */
    public String getClient() {
      return client;
    }

    /**
     * @param client
     *            the client to set
     */
    public void setClient(String client) {
      this.client = client;
    }

    /**
     * @return the url
     */
    public String getUrl() {
      return url;
    }

    /**
     * @param url
     *            the url to set
     */
    public void setUrl(String url) {
      this.url = url;
    }

    /**
     * @return the publicUrl
     */
    public String getPublicUrl() {
      return publicUrl;
    }

    /**
     * @param publicUrl the publicUrl to set
     */
    public void setPublicUrl(String publicUrl) {
      this.publicUrl = publicUrl;
    }
  }

  public static class Wayf {
    private String url = "http://example.com";

    /**
     * @return the url
     */
    public String getUrl() {
      return url;
    }

    /**
     * @param url
     *            the url to set
     */
    public void setUrl(String url) {
      this.url = url;
    }


  }

  public static class Idp {

    private String aliasPrefix = "prefix-";

    private List<Map<String, Object>> mappers = new ArrayList<>();

    /**
     * @return the aliasPrefix
     */
    public String getAliasPrefix() {
      return aliasPrefix;
    }

    /**
     * @param aliasPrefix
     *            the aliasPrefix to set
     */
    public void setAliasPrefix(String aliasPrefix) {
      this.aliasPrefix = aliasPrefix;
    }

    /**
     * @return the mappers
     */
    public List<Map<String, Object>> getMappers() {
      return mappers;
    }

    /**
     * @param mappers the mappers to set
     */
    public void setMappers(List<Map<String, Object>> mappers) {
      this.mappers = mappers;
    }
  }

  public static class Sp {
    private String uiDisplayName = "uiDisplayName";

    private String uiDisplayNameEn = "displayNameEn";

    private String description = "description";

    private String descriptionEn = "descriptionEn";

    private String infoUrl = "infoUrl";

    private String infoUrlEn = "infoUrlEn";

    private String privacyUrl = "privacyUrl";

    private String privacyUrlEn = "privacyUrlEn";

    private String logoUrl = "logoUrl";

    private String logoUrlEn = "logoUrlEn";

    private String orgName = "orgName";

    private String orgNameEn = "orgNameEn";

    private String orgDisplayName = "orgDisplayName";

    private String orgDisplayNameEn = "orgdisplayNameEn";

    private String orgUrl = "orgUrl";

    private String orgUrlEn = "orgUrlEn";

    private String contactName = "contactName";

    private String contactSurname = "contactSurname";

    private String contactEmail = "contactEmail";

    /**
     * @return the uiDisplayName
     */
    public String getUiDisplayName() {
      return uiDisplayName;
    }

    /**
     * @param uiDisplayName
     *            the uiDisplayName to set
     */
    public void setUiDisplayName(String uiDisplayName) {
      this.uiDisplayName = uiDisplayName;
    }

    /**
     * @return the uiDisplayNameEn
     */
    public String getUiDisplayNameEn() {
      return uiDisplayNameEn;
    }

    /**
     * @param uiDisplayNameEn
     *            the uiDisplayNameEn to set
     */
    public void setUiDisplayNameEn(String uiDisplayNameEn) {
      this.uiDisplayNameEn = uiDisplayNameEn;
    }

    /**
     * @return the description
     */
    public String getDescription() {
      return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
      this.description = description;
    }

    /**
     * @return the descriptionEn
     */
    public String getDescriptionEn() {
      return descriptionEn;
    }

    /**
     * @param descriptionEn
     *            the descriptionEn to set
     */
    public void setDescriptionEn(String descriptionEn) {
      this.descriptionEn = descriptionEn;
    }

    /**
     * @return the infoUrl
     */
    public String getInfoUrl() {
      return infoUrl;
    }

    /**
     * @param infoUrl
     *            the infoUrl to set
     */
    public void setInfoUrl(String infoUrl) {
      this.infoUrl = infoUrl;
    }

    /**
     * @return the infoUrlEn
     */
    public String getInfoUrlEn() {
      return infoUrlEn;
    }

    /**
     * @param infoUrlEn
     *            the infoUrlEn to set
     */
    public void setInfoUrlEn(String infoUrlEn) {
      this.infoUrlEn = infoUrlEn;
    }

    /**
     * @return the privacyUrl
     */
    public String getPrivacyUrl() {
      return privacyUrl;
    }

    /**
     * @param privacyUrl
     *            the privacyUrl to set
     */
    public void setPrivacyUrl(String privacyUrl) {
      this.privacyUrl = privacyUrl;
    }

    /**
     * @return the privacyUrlEn
     */
    public String getPrivacyUrlEn() {
      return privacyUrlEn;
    }

    /**
     * @param privacyUrlEn
     *            the privacyUrlEn to set
     */
    public void setPrivacyUrlEn(String privacyUrlEn) {
      this.privacyUrlEn = privacyUrlEn;
    }

    /**
     * @return the logoUrl
     */
    public String getLogoUrl() {
      return logoUrl;
    }

    /**
     * @param logoUrl
     *            the logoUrl to set
     */
    public void setLogoUrl(String logoUrl) {
      this.logoUrl = logoUrl;
    }

    /**
     * @return the logoUrlEn
     */
    public String getLogoUrlEn() {
      return logoUrlEn;
    }

    /**
     * @param logoUrlEn
     *            the logoUrlEn to set
     */
    public void setLogoUrlEn(String logoUrlEn) {
      this.logoUrlEn = logoUrlEn;
    }

    /**
     * @return the orgName
     */
    public String getOrgName() {
      return orgName;
    }

    /**
     * @param orgName
     *            the orgName to set
     */
    public void setOrgName(String orgName) {
      this.orgName = orgName;
    }

    /**
     * @return the orgNameEn
     */
    public String getOrgNameEn() {
      return orgNameEn;
    }

    /**
     * @param orgNameEn
     *            the orgNameEn to set
     */
    public void setOrgNameEn(String orgNameEn) {
      this.orgNameEn = orgNameEn;
    }

    /**
     * @return the orgDisplayName
     */
    public String getOrgDisplayName() {
      return orgDisplayName;
    }

    /**
     * @param orgDisplayName
     *            the orgDisplayName to set
     */
    public void setOrgDisplayName(String orgDisplayName) {
      this.orgDisplayName = orgDisplayName;
    }

    /**
     * @return the orgDisplayNameEn
     */
    public String getOrgDisplayNameEn() {
      return orgDisplayNameEn;
    }

    /**
     * @param orgDisplayNameEn
     *            the orgDisplayNameEn to set
     */
    public void setOrgDisplayNameEn(String orgDisplayNameEn) {
      this.orgDisplayNameEn = orgDisplayNameEn;
    }

    /**
     * @return the orgUrl
     */
    public String getOrgUrl() {
      return orgUrl;
    }

    /**
     * @param orgUrl
     *            the orgUrl to set
     */
    public void setOrgUrl(String orgUrl) {
      this.orgUrl = orgUrl;
    }

    /**
     * @return the orgUrlEn
     */
    public String getOrgUrlEn() {
      return orgUrlEn;
    }

    /**
     * @param orgUrlEn
     *            the orgUrlEn to set
     */
    public void setOrgUrlEn(String orgUrlEn) {
      this.orgUrlEn = orgUrlEn;
    }

    /**
     * @return the contactName
     */
    public String getContactName() {
      return contactName;
    }

    /**
     * @param contactName
     *            the contactName to set
     */
    public void setContactName(String contactName) {
      this.contactName = contactName;
    }

    /**
     * @return the contactSurname
     */
    public String getContactSurname() {
      return contactSurname;
    }

    /**
     * @param contactSurname
     *            the contactSurname to set
     */
    public void setContactSurname(String contactSurname) {
      this.contactSurname = contactSurname;
    }

    /**
     * @return the contactEmail
     */
    public String getContactEmail() {
      return contactEmail;
    }

    /**
     * @param contactEmail
     *            the contactEmail to set
     */
    public void setContactEmail(String contactEmail) {
      this.contactEmail = contactEmail;
    }
  }

  /**
   * @return the syncRate
   */
  public String getSyncRate() {
    return syncRate;
  }

  /**
   * @param syncRate the syncRate to set
   */
  public void setSyncRate(String syncRate) {
    this.syncRate = syncRate;
  }

  /**
   * @return the preferredLanguage
   */
  public String getPreferredLanguage() {
    return preferredLanguage;
  }

  /**
   * @param preferredLanguage the preferredLanguage to set
   */
  public void setPreferredLanguage(String preferredLanguage) {
    this.preferredLanguage = preferredLanguage;
  }
}
