package org.wikitolearn.shibbolethdiscovery.client;

import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.wikitolearn.shibbolethdiscovery.config.ApplicationProperties;
import org.xml.sax.SAXException;

/**
 * Service class that acts as a client.
 * Its goal is to interact with Federation resources.
 *
 * @author Alessandro Tundo
 */
@Service
public class FederationWayfClient {

  private final Logger log = LoggerFactory.getLogger(FederationWayfClient.class);
  @Autowired
  private ApplicationProperties applicationProperties;

  /**
   * Download the WAYF XML file from the Federation and store it as a DOM.
   * @return Document the DOM representation of the XML file
   * @throws RuntimeException
   */
  public Document downloadWayfFile() {
    try {
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      Document document = builder.parse(this.applicationProperties.getWayf().getUrl());
      document.getDocumentElement().normalize();
      return document;
    } catch (ParserConfigurationException | SAXException | IOException e) {
      log.error("Unable to download WAYF file", e);
      throw new RuntimeException(e);
    }
  }
}
