package org.wikitolearn.shibbolethdiscovery.web.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.wikitolearn.shibbolethdiscovery.service.MetadataService;

/**
 * REST controller that exposes the Service Provider metadata.
 *
 * @author alessandro
 */
@RestController
@RequestMapping(value = "/metadata")
public class MetadataResource {
  @Autowired
  private MetadataService metadataService;

  /**
   * Get a valid Shibboleth Service Provider XML metadata.
   * @return String XML serialization of the Service Provider metadata
   */
  @RequestMapping(method = RequestMethod.GET, produces=MediaType.APPLICATION_XML_VALUE)
  public String getMetadata() {
    return metadataService.getMetadata();
  }
}
